#include <stdio.h>
#include <stdlib.h>
#include <time.h>
int main()
{
    clock_t Time_b= clock();
    int r=1024,c=1024,i, j, k;
    int (*a)[r] = calloc(r, sizeof *a);
    int (*b)[r] = calloc(r, sizeof *b);
    int (*mul)[r] = calloc(r, sizeof *mul);
    clock_t begin_rand = clock();
    for (i = 0; i < r; i++)
    {
        for (j = 0; j < c; j++)
        {
            a[i][j] = rand() % 100;
            b[i][j] = rand() % 100;
            
        }
    }
    clock_t end_rand = clock();
    double time_spent_rand = (double)(end_rand - begin_rand) / CLOCKS_PER_SEC;
    clock_t begin_calculate = clock();
    printf("Result Matrix:\n");
    for (i = 0; i < r; i++)
    {
        for (j = 0; j < c; j++)
        {
            mul[i][j] = 0;
            for (k = 0; k < c; k++)
            {
                mul[i][j] += a[i][k] * b[k][j];
            }
        }
    }
    clock_t end_calculate = clock();
    double time_spent_calculate = (double)(end_calculate - begin_calculate) / CLOCKS_PER_SEC;
    clock_t begin_print = clock();
    // print result
    for (i = 0; i < r; i++)
    {
        for (j = 0; j < c; j++)
        {
            printf("%d\t", mul[i][j]);
        }
        printf("\n");
    }
    clock_t end_print = clock();
    double time_spent_print = (double)(end_print - begin_print) / CLOCKS_PER_SEC;
    free(a);
    free(b);
    free(mul);
    clock_t end = clock();
    double time_spent = (double)(end - Time_b) / CLOCKS_PER_SEC;
    printf("Random number into matrix(1024*1024)  %f seconds \n", time_spent_rand);
    printf("Calculate multipy matrix(1024*1024)   %f seconds \n", time_spent_calculate);
    printf("Printing matrix(1024*1024)            %f seconds \n", time_spent_print);
    printf("The program                           %f seconds \n", time_spent);
    return 0;
}
